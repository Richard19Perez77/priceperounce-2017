package price.per.ounce;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;

public class MainActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    /**
     * Called when returning to the activity
     */
    @Override
    public void onResume() {
        super.onResume();
//        Animation anim = AnimationUtils.loadAnimation(this.getBaseContext(),
//                R.anim.intro);
//        anim.reset();
//        RelativeLayout container = (RelativeLayout) findViewById(R.id.container);
//        container.clearAnimation();
//        container.startAnimation(anim);
    }
}
