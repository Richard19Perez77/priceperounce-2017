package price.per.ounce;

public class InValidData {
    public String[] price = {"-1", "a1", "1a.0", "01", "1.000"};

    public String[] ounces = {"-1", "a1", "1a.0", "01", "1.000"};

    public String pricePerOunce = "1.00";
}